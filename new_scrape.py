"""
Author : Indrajit Sadhukhan
Date Modified : 26.05.2023

"""

# Import libraries
from urllib.request import urljoin
from bs4 import BeautifulSoup
import requests
import os
from urllib.request import urlparse,urlopen

# Function to generate Folder name from Base URL
def folderName(url):
	output_str = url.replace('/','_')
	output_str = output_str.replace(':','_')
	return output_str

# input_url = "https://www.usaid.gov/" we have omit the URL as it has more than 206 URLs.

# TODO: Include links with PDFs - Sravana
url_list = ["https://www.acquisition.gov/browse/index/far","https://www.usaid.gov/partner-with-us/grant-and-contract-process","https://www.usaid.gov/about-us/agency-policy/series-300/302","https://www.usaid.gov/india/document/implementing-partners-guide-types-assistance-instruments","https://www.usaid.gov/partner-with-us/aapds-cibs"]


depth = 2
encoding = "utf-8"
lvl=0


# Set for storing urls with different domain
links_extern = set()


def remove_newlines(serie):
    serie = serie.replace('\n', ' ')
    serie = serie.replace('\\n', ' ')
    serie = serie.replace('  ', ' ')
    return serie

count=0
# Method for crawling a url at next level
def level_crawler(input_url):
	folder_name = folderName(input_url)
	if(os.path.exists("text/"+folder_name)==False):
		os.mkdir("text/"+folder_name)
	global lvl,count
	lvl+=1
	temp_urls = set()

	# Set for storing urls with same domain
	links_intern = set()

	# Creates beautiful soup object to extract html tags
	beautiful_soup_object = BeautifulSoup(
		requests.get(input_url).content, "lxml")

	# Access all anchor tags from input
	# url page and divide them into internal
	# and external categories
	
	for anchor in beautiful_soup_object.findAll("a"):
		href = anchor.attrs.get("href")
		if(href != "" or href != None):
			href = urljoin(input_url, href)
			href_parsed = urlparse(href)
			href = href_parsed.scheme
			href += "://"
			href += href_parsed.netloc
			href += href_parsed.path
			final_parsed_href = urlparse(href)
			is_valid = bool(final_parsed_href.scheme) and bool(final_parsed_href.netloc)
			if is_valid:
				# Condition to scrape only within domain.
				if href.startswith(input_url) and href not in links_intern:
					count+=1
					print("Link %d: "%(count) + href)
					links_intern.add(href)
					# Extract text from href and store it in a text_scraped folder.
					try:
						page = urlopen(href)
						htmlcontent = (page.read()).decode(encoding)
						soup = BeautifulSoup(htmlcontent,"html.parser")
						# Write the text contents in files 
						with open("text/"+folderName(input_url)+"/depth%d_%d.txt"%(lvl,count),'w',encoding = encoding ,errors="ignore") as f:
							f.write(href+"\n"+remove_newlines(soup.get_text()))
						temp_urls.add(href)
					except:
						print("Unable to open link: "+href)
	return temp_urls


if(depth == 0):
	for input_url in url_list:
		print(input_url)

elif(depth == 1):
	lvl=0
	for input_url in url_list:
		level_crawler(input_url)

else:
	# We have used a BFS approach considering the structure as a tree. It uses a queue based approach to traverse links upto a particular depth.
	queue = []
	for input_url in url_list:
		queue.append(input_url)
	for j in range(depth):
		for cnt in range(len(queue)):
			url = queue.pop(0)
			urls = level_crawler(url)
			for i in urls:
				queue.append(i)


# Improve scraping

# url = "https://www.acquisition.gov/far/part-12"
# page = urlopen(url)
# encoding = "utf-8"
# htmlcontent = (page.read()).decode(encoding)
# soup = BeautifulSoup(htmlcontent,"html.parser")
# with open("output.txt",'w',encoding=encoding,errors="ignore") as f:
# 	f.write(url+"\n"+remove_newlines(soup.get_text()))