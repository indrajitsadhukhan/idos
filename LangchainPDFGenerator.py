"""
Author : Indrajit Sadhukhan
Date Modified : 26.05.2023

Last Code clean:
26.05.2023 - 3:51 PM

TODO:
1. Include PDF Links
2. Create a function to generate FAISS Index
3. Append FAISS index with new content.
4. How to store faiss in milvus
5. Can we query faiss index?

Expemintal Ideas:
1. Perform embeddings on scraped text after passing it through gpt-3.5-turbo.
"""
import os
import openai
import json
import pandas as pd
import numpy as np
import pandas as pd
from fpdf import FPDF
import numpy as np
import time


from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.llms import OpenAI

# SURYA's API KEY
# API_KEY="39a176d4cd404a8eb33f12b01cf42d39"
# API_BASE="https://jayasai01.openai.azure.com/"
# COMPLETION_MODEL=""
# EMBEDDING_MODEL=""
# API_VERSION='2023-03-15-preview'
# API_TYPE="azure"

# Sravana's API KEY
API_KEY="f93979cbf9894257affd4fee8b4e08fb"
API_BASE="https://sravanakumar13sathish.openai.azure.com/"
COMPLETION_MODEL = "Policy_GPT"
EMBEDDING_MODEL="text-embedding-ada-002"
API_VERSION='2023-03-15-preview'
API_TYPE="azure"

openai.api_key = API_KEY
openai.api_base = API_BASE
openai.api_type=API_TYPE
openai.api_version=API_VERSION

os.environ["OPENAI_API_TYPE"] = API_TYPE
os.environ["OPENAI_API_BASE"] = API_BASE
os.environ["OPENAI_API_KEY"] = API_KEY
os.environ["OPENAI_API_VERSION"] = API_VERSION

# openai.error.InvalidRequestError: Too many inputs. The max number of inputs is 1.  We hope to increase the number of inputs per request soon. Please contact us through an Azure support request at: https://go.microsoft.com/fwlink/?linkid=2213926 for further questions.

# chunk_size=100

embeddings = OpenAIEmbeddings(model=EMBEDDING_MODEL,chunk_size=1)


df = pd.DataFrame()

# Keep all scraped data inside text folder - Use new_scrape.py to scrape
text_dir='text/'
faiss_index = "faiss_index_large"

# This function will read all scraped data, generate embeddings and store FAISS Index locally
# The text folder will contain subfolders corresponding to every URL. Each subfolder will contain multiple text files upto depth=2
def create_embeddings():
    raw_text = ""
    for folder in os.listdir(text_dir):
        for text_files in os.listdir(text_dir+folder):
            with open(text_dir+folder+"/"+text_files,'r',encoding='utf8') as r:
                raw_text += r.read()
    print("Text Extracted.")

    # chunk_size = The maximum number of characters in each chunk.
    text_splitter = CharacterTextSplitter(separator=".",chunk_size=1000,chunk_overlap=200,length_function=len)
    split_time = time.time()
    texts = text_splitter.split_text(raw_text)
    print("SPLIT TIME: %d"%(time.time()-split_time))

    embed_time=time.time()
    db = FAISS.from_texts(texts, embeddings)
    print("Embeddings created.")
    print("Embeddings Time: %d",(time.time()-embed_time))

    faiss_ind_time = time.time()
    db.save_local(faiss_index)
    print("FAISS Index saved.")
    print("FAISS Index time: %d",(time.time()-faiss_ind_time))
    return db

# Function to print context - i.e. The exact content from the scraped data using which the answer will be generated.

def print_context(docs):
    cnt=0
    for document in docs:
        cnt+=1
        doc_json=json.loads(document.json())
        print("Context %d: "%(cnt)+doc_json['page_content']+"\n")
    


# This function will load faiss_index from local and perform similarity search to find the most relevant text from the database and generate answer.
def answer_question(query):
    # Load faiss_index
    new_db = FAISS.load_local(faiss_index, embeddings)
    print("Answer Question called.")
    docs = new_db.similarity_search(query)
    # Print context
    # print_context(docs)
    chain = load_qa_chain(OpenAI(engine=COMPLETION_MODEL,temperature=0,max_tokens=1000),chain_type="stuff")
    neg_prompt = "If the answer cannot be given using the document, say \"I don't know. \""
    answer = chain.run(input_documents=docs,question=query+neg_prompt)
    return answer

# Generate text from Topic 
def get_content(search_text):
    search_text = "Write approximately 300 words on the topic "+search_text + "\n The answer should have 3 seperate paragraphs and the answer should not end halfway. Complete the last sentence. "
    result = answer_question(search_text)
    return result


# Parse the Table of Contents Excel File and generate PDF
def generate_pdf(filename):
    df = pd.read_excel(filename)
    df.drop([0,1,2,3,4,5], axis = 0, inplace = True)
    print(df)
    df.loc[len(df)+5] = ['end',0]
    df.dropna(inplace = True)
    df.reset_index(inplace=True)

    pdf = FPDF('P', 'mm', 'A4')
    pdf.add_page()
    pdf.add_font('DejaVu','','DejaVuSansCondensed.ttf',uni=True)
    pdf.set_font("DejaVu", size = 15)
    heading = ''
    sub_heading = ''
    sub_sub_heading = ''
    # parse_excel()
    print(df.index)

    for index in df.index:
        print(index)
        print(df['Unnamed: 0'][index])
        list1 = str(df['Unnamed: 0'][index]).split('.')
        if str(df['Unnamed: 0'][index]) == 'end':
            #print(index)
            break
        #print("List : ",list1)
        if len(str(df['Unnamed: 0'][index]).split('.')) == 1:
            heading = str(df['Unnamed: 0'][index]) + "   " + str(df['Unnamed: 1'][index])
            print(heading)
            if len(str(df['Unnamed: 0'][index+1]).split('.')) == 1:
                pdf.cell(200, 10, txt = heading,ln = 1, align = 'L')
                # pdf.cell(200, 10, ln = 1, txt = sample_content, align = 'L')
                sample_content = get_content(df['Unnamed: 1'][index])
                print(sample_content)
                print(df['Unnamed: 1'][index])
                pdf.multi_cell(0, 6, sample_content)
            elif len(str(df['Unnamed: 0'][index+1]).split('.')) != 1:
                pdf.cell(200, 10, txt = heading,ln = 1, align = 'L')
        elif len(str(df['Unnamed: 0'][index]).split('.')) == 2:
            sub_heading = str(df['Unnamed: 0'][index]) + "   " + str(df['Unnamed: 1'][index])
            if len(str(df['Unnamed: 0'][index+1]).split('.')) == 2 or len(str(df['Unnamed: 0'][index+1]).split('.')) == 1:
                print("2",sub_heading,index)
                pdf.cell(200, 10, txt = sub_heading, ln = 1, align = 'L')
                sample_content = get_content(df['Unnamed: 1'][index] + "In the context of "+heading)
                print(df['Unnamed: 1'][index])
                print(sample_content)
                pdf.multi_cell(0, 6, sample_content)
            elif len(str(df['Unnamed: 0'][index+1]).split('.')) != 1:
                pdf.cell(200, 10, txt = sub_heading, ln = 1, align = 'L')
                print("2con",sub_heading,index)
                continue
        elif len(str(df['Unnamed: 0'][index]).split('.')) == 3:
            print("3",index)
            sub_sub_heading = str(df['Unnamed: 0'][index]) + "   " + str(df['Unnamed: 1'][index])
            pdf.cell(200, 10, txt = sub_sub_heading,ln = 1, align = 'L')
            # pdf.cell(200, 10, ln = 1, txt = sample_content, align = 'L')
            sample_content = get_content(df['Unnamed: 1'][index] + "In the context of "+heading)
            pdf.multi_cell(0, 6, sample_content)
    pdf.output("SOP.pdf")

"""
Questions:
What is the first step in the USAID grant and contract process?
How does USAID gather information about local capacity and small businesses?
What are the different types of solicitations that USAID may issue?
What are some of the evaluation criteria used by USAID in reviewing proposals?
Who should the organization contact if they cannot reach an agreement with the Contracting or Agreement Officer?
What are the steps in the USAID grant and contract process?
"""

q1="What is the first step in the USAID grant and contract process?"
q2="How does USAID gather information about local capacity and small businesses?"
q3="What are the steps in the USAID grant and contract process?"
q4="Who should the organization contact if they cannot reach an agreement with the Contracting or Agreement Officer?"

nq1="What is GDP of India?"
nq2="Who is the prime minister of India?"
nq3="What is your name?"

# print(get_content(nq2))
# create_embeddings()
generate_pdf("TOC.xlsx")