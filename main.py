raw_text = ""
file = open("Scraped_text.txt",encoding="utf8")
raw_text+=file.read()

from PyPDF2 import PdfReader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import ElasticVectorSearch, Pinecone, Weaviate, FAISS

import os
os.environ["OPENAI_API_KEY"]=""

# Crawl 


text_splitter = CharacterTextSplitter(seperator=" ",chunk_size=1000,chunk_overlap=200,length_function=len)

texts = text_splitter.split_text(raw_text)
embeddings = OpenAIEmbeddings()
docsearch=FAISS.from_texts(texts,embeddings)
from langchain.chains.question_answering import load_qa_chain
from langchain.llms import OpenAI

chain = load_qa_chain(OpenAI(),chain_type="stuff")
query = "Write 100 words on Types of subcontracts. In the context of Subcontracts Manual"
docs = docsearch.similarity_search(query)
chain.run(input_documents=docs,question=query)
